/// <reference types="cypress" />

context('Premium', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4200');
  });

  it('should show premium offers if normal customer', () => {
    cy.get('#customerName')
      .type('Helene Birne').should('have.value', 'Helene Birne');
    cy.get('#customerId')
      .type('300000').should('have.value', '300000');

    cy.get('section form .btn.btn-primary').click();

    cy.get('.row h2.col-12').should('contain', 'Premiumangebote');
  });
});
