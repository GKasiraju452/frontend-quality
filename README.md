__Für alle Befehle vorher in den Ordner `angular-quality` gehen__

# aot build
```
ng build --prod
```
# css lint
# ts lint
```
ng lint
```
# compodoc+coverage
# unit tests mit karma
```
ng test
```
# e2e protractor
```
ng e2e
```
# jest
# testcafe
# sonarqube

Docker
docker run -u $(id -u) -v $(pwd)/angular-quality:/app --rm trion/ng-cli-karma:6.2.2 npm ci
docker run -u $(id -u) -v $(pwd)/angular-quality:/app --rm trion/ng-cli-karma:6.2.2 node puppeteer-sample.js



